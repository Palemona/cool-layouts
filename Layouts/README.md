# Cool Layouts by Pale

This is a repository of the web layouts made by Pale. 

## Terms of use

- Feel free to rewrite HTML, CSS, and Javascript as you want and modify the general appearance of the layout as you want.
- I would appreciate if you don't remove the credits, but you can move them freely to another part of the website.
- Don't redistribute the original source code and claim as yours.
---
- Terms are subject to change without notice.
- Code is subject to change without notice.
- Demo images and assets belongs to their respective authors and are credited on each work.

---

If you're looking for tumblr themes, please go this [link](https://gitlab.com/Palemona/tumblr-themes) .
