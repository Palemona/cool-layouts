    $(document).ready(function () {

        if (window.innerWidth < 769) {
            $(".sidenav").addClass("nav_active");
        } else {
            $(window).on("scroll", function () {
                if ($(window).scrollTop() > 300) {
                    $(".sidenav").addClass("nav_active");
                } else {
                    $(".sidenav").removeClass("nav_active");
                }
            });
        }
        $(window).on("scroll", function () {
            scrollFunction();
        });


        var showMenu = false;
        $('#menu_open, #closemenu').click(function () {
            showMenu = !showMenu;
            if (showMenu) {
                $("#sidemenu").css("transform", "translateX(0px)");
            } else {
                $("#sidemenu").css("transform", "translateX(-250px)");
            }
        });
    });

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("top_button").style.display = "flex";
        } else {
            document.getElementById("top_button").style.display = "none";
        }
    }

    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    var example1 = new Vue({
        el: '#themes',
        data: {
            sort: true,
            themes: [
                {
                    name: "Glassy",
                    type: "Theme Layout",
                    tags: "responsive, about section, dynamic, gallery",
                    img: "https://gitlab.com/Palemona/cool-layouts/-/raw/main/assets/glassy.webm",
                    prev1: "Layouts/Glassy/dynamic_ver/index.html",
                    code: "https://ko-fi.com/s/b0cf5ad8bb"
                }
            ],
            search: ''
        },
        computed: {
            filteredItems() {
                if (this.sort) {
                    return this.themes.slice().filter(item => {
                        return item.type.indexOf(this.search.toLowerCase()) > -1
                    })
                } else {
                    return this.themes.slice().filter(item => {
                        return item.type.indexOf(this.search.toLowerCase()) > -1
                    })
                }
            },
        }

    })
